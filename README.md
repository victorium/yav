# Yet Another Validator (YAV) for PHP

# Introduction

YAV is a very simple, stupid implementation framework agnostic library to validate and sanitize data.

## Licence

All source files in YAV are released subject to the terms of the MIT license, as written in the included LICENSE.txt file.

## Installation

### Composer

YAV can be installed with composer (http://getcomposer.org/).

1. Install composer:

	```
	$ curl -s https://getcomposer.org/installer | php
	```

2. Require YAV as a dependency using composer:

	```
	$ php composer.phar require victorium/yaqb
	```

## Getting Started

This is a simple example on getting started with YAV:

	<?php

    define("PATH_TO_YAV_SRC", dirname(__DIR__));

    require PATH_TO_YAV_SRC . "/bootstrap.php";

    use Yav\Processor\StrProcessor;

    // all error messages are stored into here
    $errors = [];

    $myName = "\nJohn Doe";

    $myName = StrProcessor::init(@$myName)
                ->required()
                ->strip()
                ->satisfies("/\s/")
                ->errorMessages("myName", $errors, [
                    "required" => "Please enter your name",
                    "satisfies" => "Please enter your full name",
                ])->value();


## Features

YAV supports the following features:

### String processing

    <?php

    $data["place"] = StrProcessor::init(@$rawData["place"])
                ->required()
                ->strip()
                ->satisfies("/,/")
                ->errorMessages("place", $errors, [
                    "required" => "Please enter the city and country",
                    "satisfies" => "Please enter in the format: city,country",
                ])->value();

    $data["country_code"] = StrProcessor::init(@$rawData["country_code"])
                ->required()
                ->maxLength(2)
                ->minLength(2)
                ->registerCallback(function (&$data, &$errors) {
                    $data = strtoupper($data);
                })->errorMessages("country_code", $errors, [
                    "required" => "Please enter the country code",
                    "maxLength" => "Country codes are 2 letters",
                    "minLength" => "Country codes are 2 letters",
                ])->value();

### Integer and float processing

### Date and time processing

### Email processing

### Phone number processing

### File processing
