<?php

define("PATH_TO_YAV_SRC", dirname(__DIR__));

require PATH_TO_YAV_SRC . "/bootstrap.php";

use Yav\Processor\StrProcessor;

// all error messages
$errors = [];
$data = [];
$rawData = [
    "country_code" => "99i",
];

$myName = "\nJohn Doe";

$myName = StrProcessor::init(@$myName)
            ->required()
            ->strip()
            ->satisfies("/\s/")
            ->errorMessages("myName", $errors, [
                "required" => "Please enter your name",
                "satisfies" => "Please enter your full name",
            ])->value();

    $data["place"] = StrProcessor::init(@$rawData["place"])
                ->required()
                ->strip()
                ->satisfies("/,/")
                ->errorMessages("place", $errors, [
                    "required" => "Please enter the city and country",
                    "satisfies" => "Please enter in the format: city,country",
                ])->value();

    $data["country_code"] = StrProcessor::init(@$rawData["country_code"])
                ->required()
                ->maxLength(2)
                ->minLength(2)
                ->satisfies("/^[a-zA-Z]$/")
                ->registerCallback(function (&$data, &$errors) {
                    $data = strtoupper($data);
                })->errorMessages("country_code", $errors, [
                    "required" => "Please enter the country code",
                    "maxLength" => "Country codes are 2 letters",
                    "minLength" => "Country codes are 2 letters",
                    "satisfies" => "Country codes are 2 letters",
                ])->value();