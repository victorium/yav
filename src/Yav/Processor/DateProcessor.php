<?php

namespace Yav\Processor;

use Yav\Processor\Processor;
use DateTime;

/**
 * Value processor for dates
 *
 * Copyright Victorium Holdings, 2016, All rights reserved
 * Written by Joseph N. Mutumi
 */
class DateProcessor extends Processor {

    protected $format = "Y-m-d";
    protected $minimumValue = null;
    protected $maximumValue = null;

    function __construct($data) {
        if ($data !== null) {
            $data = filter_var(trim($data), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_LOW | FILTER_FLAG_NO_ENCODE_QUOTES);
        }
        if ($data == "") {
            $data = null;
        }
        $this->data = $data;
    }

    /**
     * Set format used to create DateTime instance
     *
     * @param string $format The format to use
     * @return $this
     */
    public function setFormat($format) {
        $this->format = $format;
        return $this;
    }

    /**
     * Make sure value is greater than this minimum
     *
     * @param DateTime $value The minimum value to check
     * @return $this
     */
    public function minValue(DateTime $value) {
        $this->minimumValue = $value;
        return $this;
    }

    /**
     * Makes sure the value is less than this maximum
     *
     * @param DateTime $value The maximum value to check against
     * @return $this
     */
    public function maxValue(DateTime $value) {
        $this->maximumValue = $value;
        return $this;
    }

    function isValid() {
        $this->valid = true;
        $date = \DateTime::createFromFormat($this->format, $this->data);

        if ($date !== false) {
            $this->data = $date->format($this->format);
        }

        if ($this->requiredFlag !== null && $this->requiredFlag) {
            $v = strlen($this->data) > 0;
            $this->valid &= $v;
            if (!$v) {
                $this->errors["required"] = true;
            }
        }

        $v = $date !== false;
        $this->valid &= $v;
        if (!$v && $this->requiredFlag !== false && $this->data !== null) {
            $this->errors["date"] = true;
        }

        if ($this->minimumValue !== null) {
            $v = $date >= $this->minimumValue;
            $this->valid &= $v;
            if (!$v && $this->requiredFlag !== false && $this->data !== null) {
                $this->errors["minValue"] = true;
            }
        }

        if ($this->maximumValue !== null && $d) {
            $v = $date <= $this->maximumValue;
            $this->valid &= $v;
            if (!$v && $this->requiredFlag !== false && $this->data !== null) {
                $this->errors["maxValue"] = true;
            }
        }

        foreach ($this->callbacks as $callback) {
            $callback($this->data, $this->populatedErrorMessages);
        }

        $this->populateErrorMessages();

        return (bool) $this->valid;
    }


}
