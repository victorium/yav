<?php

namespace Yav\Processor;

use Yav\Processor\Processor;

/**
 * Value processor for emails
 *
 * Copyright Victorium Holdings, 2016, All rights reserved
 * Written by Joseph N. Mutumi
 */
class EmailProcessor extends Processor {

    public function __construct($data) {
        if ($data !== null) {
            $data = filter_var(trim($data), FILTER_SANITIZE_EMAIL);
        }
        $this->data = $data;
    }

    /**
     * @return bool
     */
    public function isValid() {
        $this->valid = true;
        $email = filter_var($this->data, FILTER_VALIDATE_EMAIL);

        if ($email !== false) {
            $this->data = $email;
        }

        if ($this->requiredFlag !== null && $this->requiredFlag) {
            $v = strlen($this->data) > 0;
            $this->valid &= $v;
            if (!$v) {
                $this->errors["required"] = true;
            }
        }

        $v = $email !== false;
        $this->valid &= $v;
        if (!$v && $this->requiredFlag !== false && $this->data !== null) {
            $this->errors["email"] = true;
        }

        foreach ($this->callbacks as $callback) {
            $callback($this->data, $this->populatedErrorMessages);
        }

        $this->populateErrorMessages();

        return (bool) $this->valid;
    }
}
