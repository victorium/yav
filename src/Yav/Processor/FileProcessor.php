<?php

namespace Yav\Processor;

use Yav\Processor\Processor;

/**
 * Value processor for a file as defined in $_FILES
 *
 * Copyright Victorium Holdings, 2016, All rights reserved
 * Written by Joseph N. Mutumi
 */
class FileProcessor extends Processor {

    public function __construct($data) {
        $this->data = $data;
    }

    /**
     * Whether the value is valid
     *
     * @return bool
     */
    function isValid() {
        $valid = true;

        if ($this->data !== null) {
            if (array_key_exists("error", $this->data) && $this->data["error"]) {
                if ($this->data["error"] == 4 && $this->requiredFlag === false) {
                    // continue
                } else {
                    switch ($this->data["error"]) {
                        case 1: $this->errors["max_upload_size"] = 1; break;
                        case 2: $this->errors["max_file_size"] = 1; break;
                        case 3: $this->errors["partial_upload"] = 1; break;
                        case 4: $this->errors["no_file"] = 1; break;
                        case 6: $this->errors["no_tmp_dir"] = 1; break;
                        case 7: $this->errors["cant_write"] = 1; break;
                        case 8: $this->errors["ext_blocked"] = 1; break;
                    }
                }
            }
        }

        if ($this->requiredFlag !== null && $this->requiredFlag) {
            $v = is_file($this->data["tmp_name"]);
            $valid &= $v;
            if (!$v) {
                $this->errors["required"] = true;
            }
        }

        foreach ($this->callbacks as $callback) {
            $callback($this->data, $this->populatedErrorMessages);
        }

        $this->populateErrorMessages();

        return (bool) $valid;
    }
}
