<?

namespace Yav\Processor;

use Yav\Processor\Processor;

/**
 * Value processor for floats
 *
 * Copyright Victorium Holdings, 2016, All rights reserved
 * Written by Joseph N. Mutumi
 */
class FloatProcessor extends Processor {

    protected $minimumValue = null;
    protected $maximumValue = null;
    protected $floats = null;

    public function __construct($data) {
        if (is_string($data)) {
            $data = trim($data);
            if ($data === "") {
                $data = null;
            }
        }
        if ($data !== null) {
            $data = (float) filter_var($data, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION | FILTER_FLAG_ALLOW_SCIENTIFIC);
        }
        $this->data = $data;
    }

    /**
     * Make sure value is greater than or equal to this minimum
     *
     * @param float $value The minimum value to check
     * @return $this
     */
    public function minValue($value) {
        $this->minimumValue = (float) $value;
        return $this;
    }

    /**
     * Makes sure the value is less than or equal to this maximum
     *
     * @param float $value The maximum value to check against
     * @return $this
     */
    public function maxValue($value) {
        $this->maximumValue = (float) $value;
        return $this;
    }

    /**
     * Make sure the value is one of these floats
     *
     * @param array $floats Array of floats to check against
     * @return $this;
     */
    public function isFloats(array $floats) {
        $this->floats = array_flip($floats);
        return $this;
    }

    public function isValid() {
        $this->valid = true;

        if ($this->requiredFlag !== null && $this->requiredFlag) {
            $v = $this->data !== null;
            $this->valid &= $v;
            if (!$v) {
                $this->errors["required"] = true;
            }
        }

        if ($this->minimumValue !== null) {
            $v = $this->data >= $this->minimumValue;
            $this->valid &= $v;
            if (!$v && $this->requiredFlag !== false && $this->data !== null) {
                $this->errors["minimumValueue"] = true;
            }
        }

        if ($this->maximumValue !== null) {
            $v = $this->data <= $this->maximumValue;
            $this->valid &= $v;
            if (!$v && $this->requiredFlag !== false && $this->data !== null) {
                $this->errors["maxValue"] = true;
            }
        }

        if ($this->floats !== null) {
            $v = array_key_exists($this->data, $this->floats);
            $this->valid &= $v;
            if (!$v && $this->requiredFlag !== false && $this->data !== null) {
                $this->errors["isFloats"] = true;
            }
        }

        foreach ($this->callbacks as $callback) {
            $callback($this->data, $this->populatedErrorMessages);
        }

        $this->populateErrorMessages();

        return (bool) $this->valid;
    }
}
