<?php

namespace Yav\Processor;

/**
 * Interface for validators and value objects
 *
 * Copyright Victorium Holdings, 2016, All rights reserved
 * Written by Joseph N. Mutumi
 */
abstract class Processor {

    protected $name = null;
    protected $data = null;
    protected $populatedErrorMessages = null;
    protected $messages = null;
    protected $errors = [];
    protected $callbacks = [];
    protected $requiredFlag = null;
    protected $valid = null;

    public abstract function isValid();

    /**
     * Short form for constructor
     * @return $this
     */
    public static function init(...$args) {
        return new static(...$args);
    }

    /**
     * Register a callback function for much more validation options
     *
     * @param callback $callback Callback function that takes $this as argument for more validation options
     * @return $this
     */
    public function registerCallback($callback) {
        $this->callbacks[] = $callback;
        return $this;
    }

    /**
     * Make the value required
     *
     * @param bool|null $status Whether it should be required, default is true
     * @return $this instance of this class
     */
    public function required($status = true) {
        $this->requiredFlag = (bool) $status;
        return $this;
    }

    /**
     * Set the first error message for the field name
     *
     * @param str $name The name of the field
     * @param array &$errors The errors array to populate
     * @param array $messages
     * @return $this
     */
    public function errorMessages($name, array &$errors, array $messages) {
        $this->name = $name;
        $this->populatedErrorMessages = &$errors;
        $this->messages = $messages;
        return $this;
    }

    /**
     * The sanitized data
     * @return string
     */
    public function value() {
        if (!is_bool($this->valid)) {
            $this->isValid();
        }
        return $this->rawValue();
    }

    /**
     * The unsanitized data
     * @return string
     */
    function rawValue() {
        return $this->data;
    }

    protected function populateErrorMessages() {
        foreach ($this->errors as $key => $invalid) {
            if ($invalid && isset($this->messages[$key])) {
                $this->populatedErrorMessages[$this->name] = $this->messages[$key];
                break;
            }
        }
        return $this;
    }
}
