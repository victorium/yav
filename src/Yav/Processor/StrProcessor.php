<?php

namespace Yav\Processor;

use Yav\Processor\Processor;

/**
 * Value processor for strings
 *
 * Copyright Victorium Holdings, 2016, All rights reserved
 * Written by Joseph N. Mutumi
 */
class StrProcessor extends Processor {

    protected $stripStringOf = null;
    protected $minimumLength = null;
    protected $maximumLength = null;
    protected $satisfiesRegex = null;
    protected $strings = null;

    public function __construct($data) {
        if ($data !== null) {
            $data = filter_var($data, FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
        }
        $this->data = $data;
    }

    /**
     * Remove specific characters from string before processing
     *
     * @param string $characterMask The characters to be stripped
     * @return $this
     */
    public function strip($characterMask = " \t\n\r\0\x0B") {
        $this->stripStringOf = $characterMask;
        return $this;
    }

    /**
     * Make sure string length is greater than or equal to this minimum
     *
     * @param int $length The minimum value to check
     * @return $this
     */
    public function minLength($length) {
        $this->minimumLength = (int) $length;
        return $this;
    }

    /**
     * Make sure string length is less than or equal to this maximum
     *
     * @param int $length The minimum value to check
     * @return $this
     */
    public function maxLength($length) {
        $this->maximumLength = (int) $length;
        return $this;
    }

    /**
     * Make sure string satisfies regular expression
     *
     * @param string $regex The regular expression to check with
     * @return $this
     */
    public function satisfies($regex) {
        $this->satisfiesRegex = $regex;
        return $this;
    }

    /**
     * Makes sure the value is one of these strings
     *
     * @param array $strings Array of strings to test against
     */
    public function isStrings(array $strings) {
        $this->strings = array_flip($strings);
        return $this;
    }

    /**
     * Whether the value is valid
     *
     * @return bool
     */
    function isValid() {
        $this->valid = true;

        if ($this->stripStringOf !== null) {
            $this->data = trim($this->data, $this->stripStringOf);
        }

        if ($this->requiredFlag !== null && $this->requiredFlag) {
            $v = strlen($this->data) > 0;
            $this->valid &= $v;
            if (!$v) {
                $this->errors["required"] = true;
            }
        } elseif ($this->requiredFlag === false && $this->data === "") {
            $this->data = null;
        }

        if ($this->minimumLength !== null) {
            $v = strlen($this->data) >= $this->minimumLength;
            $this->valid &= $v;
            if (!$v && $this->requiredFlag !== false && $this->data !== null) {
                $this->errors["minLength"] = true;
            }
        }

        if ($this->maximumLength !== null) {
            $v = strlen($this->data) <= $this->maximumLength;
            $this->valid &= $v;
            if (!$v && $this->requiredFlag !== false && $this->data !== null) {
                $this->errors["maxLength"] = true;
            }
        }

        if ($this->satisfiesRegex !== null) {
            $v = preg_match($this->satisfiesRegex, $this->data) === 1;
            $this->valid &= $v;
            if (!$v && $this->requiredFlag !== false && $this->data !== null) {
                @$this->errors["satisfies"] = !$v;
            }
        }

        if ($this->strings !== null) {
            $v = array_key_exists($this->data, $this->strings);
            $this->valid &= $v;
            if (!$v && $this->requiredFlag !== false && $this->data !== null) {
                $this->errors["isStrings"] = true;
            }
        }

        foreach ($this->callbacks as $callback) {
            $callback($this->data, $this->populatedErrorMessages);
        }

        $this->populateErrorMessages();

        return (bool) $this->valid;
    }
}
