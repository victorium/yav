<?

namespace Yav\Processor;

use Yav\Processor\Processor;

/**
 * Value processor for integers
 *
 * Copyright Victorium Holdings, 2016, All rights reserved
 * Written by Joseph N. Mutumi
 */
class IntProcessor extends Processor {

    protected $minimumValue = null;
    protected $maximumValue = null;
    protected $integers = null;

    public function __construct($data) {
        if (is_string($data)) {
            $data = trim($data);
            if ($data === "") {
                $data = null;
            }
        }
        if ($data !== null) {
            $data = (int) filter_var($data, FILTER_SANITIZE_NUMBER_INT);
        }
        $this->data = $data;
    }

    /**
     * Make sure value is greater than or equal to this minimum
     *
     * @param int $value The minimum value to check
     * @return $this
     */
    public function minValue($value) {
        $this->minimumValue = (int) $value;
        return $this;
    }

    /**
     * Makes sure the value is less than or equal to this maximum
     *
     * @param int $value The maximum value to check against
     * @return $this
     */
    public function maxValue($value) {
        $this->maximumValue = (int) $value;
        return $this;
    }

    /**
     * Make sure the value is one of these integers
     *
     * @param array $integers Array of integers to check against
     * @return $this;
     */
    public function isIntegers(array $integers) {
        $this->integers = array_flip($integers);
        return $this;
    }

    public function isValid() {
        $this->valid = true;

        if ($this->requiredFlag !== null && $this->requiredFlag) {
            $v = $this->data !== null;
            $this->valid &= $v;
            if (!$v) {
                $this->errors["required"] = true;
            }
        }

        if ($this->minimumValue !== null) {
            $v = $this->data >= $this->minimumValue;
            $this->valid &= $v;
            if (!$v && $this->requiredFlag !== false && $this->data !== null) {
                $this->errors["minimumValueue"] = true;
            }
        }

        if ($this->maximumValue !== null) {
            $v = $this->data <= $this->maximumValue;
            $this->valid &= $v;
            if (!$v && $this->requiredFlag !== false && $this->data !== null) {
                $this->errors["maxValue"] = true;
            }
        }

        if ($this->integers !== null) {
            $v = array_key_exists($this->data, $this->integers);
            $this->valid &= $v;
            if (!$v && $this->requiredFlag !== false && $this->data !== null) {
                $this->errors["isIntegers"] = true;
            }
        }

        foreach ($this->callbacks as $callback) {
            $callback($this->data, $this->populatedErrorMessages);
        }

        $this->populateErrorMessages();

        return (bool) $this->valid;
    }
}
