<?php

namespace Yav\Error;

/**
 * Dynamically build an error message at call time
 *
 * Copyright Victorium Holdings, 2016, All rights reserved
 * Written by Joseph N. Mutumi
 */
class Str {
    protected $callable;

    public function __construct($callable) {
        $this->callable = $callable;
    }

    public function __toString() {
        $callable = $this->callable;
        return $callable();
    }
}